<?php

namespace app\controllers;

use app\models\PeoplesSearch;
use Yii;
use app\models\People;

class PeopleController extends \yii\web\Controller
{
    public function actionIndex()
    {
        $model = People::find()->all();
        return $this->render('index', ['model' => $model]);
    }


    public function actionCreate()
    {
        $model = new People();

        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate()) {
                // form inputs are valid, do something here
                $model->save();
                $model = People::find()->all();
                return $this->render('index', ['model'=>$model]);
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function actionSearch()
    {
        $model = new People();
        $postData = Yii::$app->request->post();
        if($postData){
            $tag = $postData['People']['tag'];
//            var_dump($postData);
//            die();
            $modelPeople = People::find()->where(['tag'=>$tag])->all();

            return $this->render('search-confirm', ['model' => $modelPeople]);


        }
        //$model->search([$model->formName()=> ['name'=> 'hien']]);
        //var_dump(Yii::$app->request->post()['PeoplesSearch']['tag']);
        //var_dump($model);

        return $this->render('search', [
            'model' => $model,
        ]);
    }

    public function actionEdit($id){
        $model = People::findOne($id);
        $post = Yii::$app->request->post();
        if($post){
            $model->tag = $post['People']['tag'];
            $model->name = $post['People']['name'];
            if($model->validate()){
                if($model->save()){
                    $modelNew = People::find()->all();
                    return $this->render('index',[
                        'model' => $modelNew,
                    ]);
                }
            }
        }
        return $this->render('create',[
            'model' => $model,
        ]);
    }

//    public function actionGetdemo($id){
//        var_dump($id);die();
//    }

    public function actionDelete($id){
        $model = People::findOne($id);
        if($model == null){
            $model = People::find()->all();
            return $this->render('index', [
                'model'=>$model,
            ]);
        }
        $post = Yii::$app->request->post();
        if($post){
            //var_dump($post);die();
            if(isset($post['yes_button'])){
                if($model->delete()){
                    $modelNew = People::find()->all();
                    return $this->render('index',[
                        'model' => $modelNew,
                    ]);
                }
            }
            if(isset($post['no_button'])){

                    $modelNew = People::find()->all();
                    return $this->render('index',[
                        'model' => $modelNew,
                    ]);

            }
        }
        return $this->render('delete-confirm', [
            'model'=> $model,
        ]);
    }
}
