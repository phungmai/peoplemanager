<?php

namespace app\controllers;

use app\models\Students;
use Yii;

class StudentsController extends \yii\web\Controller
{
    public function actionIndex()
    {
        $model = Students::find()->all();
        $dataView = [
            'model'=>$model
        ];
        return $this->render('index',$dataView);
    }

    public function actionCreate()
    {
        $model = new Students();

        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate()) {
                $model->save();
                // form inputs are valid, do something here
                $newStudentModel = Students::find()->all();
                return $this->render('index',[
                    'model' => $newStudentModel,
                ]);
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }
}
