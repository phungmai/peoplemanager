<?php

namespace app\controllers;

use app\models\Name;
use Yii;

class NameController extends \yii\web\Controller
{
    public function actionIndex()
    {
        $model = Name::find()->all();
        return $this->render('index', ['model'=> $model]);
    }


    public function actionCreate()
    {
        $model = new Name();

        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate()) {
                // form inputs are valid, do something here
                $model->save();
                return ;
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }


}