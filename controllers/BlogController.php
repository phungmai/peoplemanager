<?php

namespace app\controllers;

use app\models\Blog;
use Yii;
class BlogController extends \yii\web\Controller
{
    public function actionIndex()
    {
        $model = Blog::find()->all();
        return $this->render('index', ['model'=> $model]);
    }


    public function actionCreate()
    {
        $model = new Blog();

        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate()) {
                // form inputs are valid, do something here
                $model->save();
                return ;
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }


}
